import { RouterModule, Router, Routes } from '@angular/router';
import { CadastroOrcamento } from './pages/cadastro.orcamento/cadastro.orcamento';
import { CadastroCliente } from './pages/cadastro.cliente/cadastro.cliente';
import { Principal } from './pages/principal/principal';

const appRoutes: Routes = [

    { path: '', component: Principal },
    { path: 'cadastroOrcamento', component: CadastroOrcamento },
    { path: 'cadastroCliente', component: CadastroCliente },
    { path: '**', component: Principal }
];

export const routing = RouterModule.forRoot(appRoutes);