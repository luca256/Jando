import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CadastroOrcamento } from './pages/cadastro.orcamento/cadastro.orcamento';
import { CadastroCliente } from './pages/cadastro.cliente/cadastro.cliente';
import { routing } from './app.routes';
import { Principal } from './pages/principal/principal';
import { AppComponent } from './app.component';
import { TextMaskModule } from 'angular2-text-mask';


@NgModule({
  declarations: [AppComponent, CadastroOrcamento, Principal, CadastroCliente],
  imports: [BrowserModule, routing, TextMaskModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
