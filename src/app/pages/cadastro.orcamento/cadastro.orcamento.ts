import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    styleUrls: ['./cadastro.orcamento.scss'],
    selector: 'cadastro.orcamento',
    templateUrl: './cadastro.orcamento.html'
})
export class CadastroOrcamento {
    maskFone: any[] = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
    maskCPFCNPJ: any[] = [/[1-9]/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];

    placeHolder = 'CPF do cliente';

    public registros = [{
        quantidade: 1,
        descricao: 'teste',
        precounitario: 5.20,
        total: 5.20
    }, {
        quantidade: 1,
        descricao: 'teste',
        precounitario: 5.20,
        total: 5.20
    }, {
        quantidade: 1,
        descricao: 'teste',
        precounitario: 5.20,
        total: 5.20
    }, {
        quantidade: 1,
        descricao: 'teste',
        precounitario: 5.20,
        total: 5.20
    }, {
        quantidade: 1,
        descricao: 'teste',
        precounitario: 5.20,
        total: 5.20
    }, {
        quantidade: 1,
        descricao: 'teste',
        precounitario: 5.20,
        total: 5.20
    }, {
        quantidade: 1,
        descricao: 'teste',
        precounitario: 5.20,
        total: 5.20
    }, {
        quantidade: 1,
        descricao: 'teste',
        precounitario: 5.20,
        total: 5.20
    }];

    public radioChange() {
        debugger
        if (this.placeHolder == 'CPF do cliente') {
            this.placeHolder = 'CNPJ do cliente';
            this.maskCPFCNPJ = [/[1-9]/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/];
        } else {
            this.placeHolder = 'CPF do cliente';
            this.maskCPFCNPJ = [/[1-9]/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];
        }
    }
}