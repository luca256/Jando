import { Component } from '@angular/core';

const { ipcRenderer } = require('electron');

@Component({
    moduleId: module.id,
    styleUrls: ['./cadastro.cliente.scss'],
    selector: 'cadastro.cliente',
    templateUrl: './cadastro.cliente.html'
})
export class CadastroCliente {
    maskFone: any[] = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
    maskCPFCNPJ: any[] = [/[1-9]/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];

    placeHolder = 'CPF do cliente';

    public blurTelefone(a, b) {
        a.style = "border: 1px solid red"
    }

    public radioChange() {
        debugger
        if (this.placeHolder == 'CPF do cliente') {
            this.placeHolder = 'CNPJ do cliente';
            this.maskCPFCNPJ = [/[1-9]/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/];
        } else {
            this.placeHolder = 'CPF do cliente';
            this.maskCPFCNPJ = [/[1-9]/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];
        }
    }
}