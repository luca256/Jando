import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    styleUrls: ['./principal.scss'],
    selector: 'principal',
    templateUrl: './principal.html'
})
export class Principal { }